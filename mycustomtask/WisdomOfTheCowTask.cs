﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace mycustomtask;
public class WisdomOfTheCowTask : Microsoft.Build.Utilities.Task
{

    // This is a input property, which can be set from the calling task.
//    [Required] // this makes a property required by msbuild (won't start task if no value is provided)
    public string Wisdom {get; set;} = "";
    // Possible parameter types are string, bool, ITaskItem and ITaskItem[] (if a property is of another type a transformation can be applied)

    [Output]
    public string CompleteCowWisdom {get; private set;} = "muhh";

    public override bool Execute()
    {
        if (string.IsNullOrEmpty(Wisdom)) {
            Wisdom = "Be excellent to each other!";
        }

        CompleteCowWisdom = Wisdom + @"
           (    )
            (oo)
   )\.-----/(O O)
  # ;       / u
    (  .   |} )
     |/ `.;|/;
     ""     "" ""
        ";

        this.Log.LogMessage(MessageImportance.High, CompleteCowWisdom);

        // indicate if task execution was successful
        return true;
    }
}
