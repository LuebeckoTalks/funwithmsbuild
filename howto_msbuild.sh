#!/bin/bash

echo "Most likely you will use msbuild via your IDE or your CI solution. But truth you will only find in CLI!"

dotnet --version

# example: build a solution
dotnet build msbuild.sln

# note: even though it is called "dotnet build" it resolves to calling "dotnet msbuild -restore".
# another note: the cli is very powerful you setup and mange your whole solution and all of its projects and dependecies by simple cli commands (no IDE needed)
# - just hit "dotnet --help"